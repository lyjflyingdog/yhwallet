//
//  FDViewController.m
//  YHWallet
//
//  Created by KamishiroAlice on 12/11/2017.
//  Copyright (c) 2017 KamishiroAlice. All rights reserved.
//

#import "FDViewController.h"
#import <FDWallet/FDWalletModules.h>
@interface FDViewController ()

@end

@implementation FDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)presentToWalletVC{
    NSString *localBundleID = [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleIdentifierKey];
    [FDWalletModules initWithBundleID:localBundleID appkey:@"xxxxxxx" finished:^(BOOL success, NSError *error) {
        if (!success || error) {
            NSLog(@"error = %@",error);
            return;
        }
        UIViewController *vc = [FDWalletModules walletVCWithMobile:@"132xxxxxxxx"];
        [self presentViewController:vc animated:YES completion:nil];
    }];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [self presentToWalletVC];
//    UIViewController *vc = [FDWalletModules walletVCWithMobile:@"用户手机号码"];
//    [self presentViewController:vc animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
