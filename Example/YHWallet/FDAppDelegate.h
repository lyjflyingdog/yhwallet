//
//  FDAppDelegate.h
//  YHWallet
//
//  Created by KamishiroAlice on 12/11/2017.
//  Copyright (c) 2017 KamishiroAlice. All rights reserved.
//

@import UIKit;

@interface FDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
