//
//  main.m
//  YHWallet
//
//  Created by KamishiroAlice on 12/11/2017.
//  Copyright (c) 2017 KamishiroAlice. All rights reserved.
//

@import UIKit;
#import "FDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FDAppDelegate class]));
    }
}
