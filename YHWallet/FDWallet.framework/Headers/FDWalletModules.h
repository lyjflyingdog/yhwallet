//
//  FDWalletModules.h
//  FDWallet
//
//  Created by sppl on 2017/11/7.
//

#import <Foundation/Foundation.h>

typedef void(^finishedBlock)(BOOL success,NSError *error);

@interface FDWalletModules : NSObject
+ (void)initWithBundleID:(NSString *)bundleID appkey:(NSString *)appkey finished:(finishedBlock)finished;
+ (UIViewController *)walletVCWithMobile:(NSString *)mobile;
@end
