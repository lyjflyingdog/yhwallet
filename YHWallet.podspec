#
# Be sure to run `pod lib lint YHWallet.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YHWallet'
  s.version          = '0.11.0'
  s.summary          = '友钱了SDK'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/lyjflyingdog/yhwallet'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'KamishiroAlice' => 'lyjflyingdog@163.com' }
  s.source           = { :git => 'https://lyjflyingdog@bitbucket.org/lyjflyingdog/yhwallet.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.vendored_frameworks = 'YHWallet/FDWallet.framework','YHWallet/thirdlib/baiduocr/AipBase.framework','YHWallet/thirdlib/baiduocr/AipOcrSdk.framework','YHWallet/thirdlib/DKNightVersion.framework'
  s.source_files = 'YHWallet/FDWallet.framework/**/*.h'
  s.dependency 'AFNetworking', '~> 3.1.0'
  s.dependency 'MJExtension'
  s.dependency 'YYWebImage'
  s.dependency 'ReactiveCocoa', '~> 2.5'
  s.dependency 'Reachability'
  s.dependency 'SVProgressHUD', '~> 2.2.2'
  s.dependency 'CYLTabBarController'
  s.dependency 'IQKeyboardManager'
  s.dependency 'UITableView+FDTemplateLayoutCell'
  s.dependency 'MJRefresh'

  s.framework = 'JavaScriptCore'
  s.framework = 'CoreGraphics'
  s.pod_target_xcconfig = { 'ENABLE_BITCODE' => 'NO' }
end
