# YHWallet

[![Version](https://img.shields.io/cocoapods/v/YHWallet.svg?style=flat)](http://cocoapods.org/pods/YHWallet)
[![License](https://img.shields.io/cocoapods/l/YHWallet.svg?style=flat)](http://cocoapods.org/pods/YHWallet)
[![Platform](https://img.shields.io/cocoapods/p/YHWallet.svg?style=flat)](http://cocoapods.org/pods/YHWallet)

本SDK是友禾公司开发、方便其他开发者接入友钱包功能。

## 系统要求

- iOS 8.0+

## 接入方法

1. 向友禾公司提供**bundle id**生成**APPKEY**。
2. 若需使用银行卡、身份证识别功能，请注册[百度云](https://cloud.baidu.com/product/imagerecognition)开发者账号，填写bundle id新建应用后，下载license文件，并命名为"**百度文字识别.license**"，添加进工程中。（本步骤可忽略）
3. 通过FDWalletModules.h文件中提供的方法，传入**BundleID**及**APPKEY**，初始化SDK，然后传入**手机号**初始化控制器，用**present**的方式加载控制器即可。
4. 打开Xcode的Build Settings，**Enable Bitcode**设为No。

## 安装

### CocoaPods

1. 在 Podfile 中添加 `pod 'YHWallet'`。
2. 执行 `pod install` 或 `pod update`。
3. 导入 \<YHWallet/FDWalletModules.h\>。

## 作者

KamishiroAlice, lyjflyingdog@163.com 

## 截图

![D530E7F3-983B-4C61-BADE-4E54547FA240](https://ws1.sinaimg.cn/large/006tKfTcgy1fmxg0s6xncj30o21bcgpa.jpg)

![A6AF02FA-BC6A-4109-8940-EC592E5D4D41](https://ws3.sinaimg.cn/large/006tKfTcgy1fmxg0r6ztlj30o21bcmzd.jpg)

![38ACEFAF-7592-45CD-A295-1127F33D0159](https://ws2.sinaimg.cn/large/006tKfTcgy1fmxg0tt3tkj30o21bctb3.jpg)

![75A412E8-8E06-4859-9F78-9B94CAD47F01](https://ws3.sinaimg.cn/large/006tKfTcgy1fmxg0uiasgj30o21bcgo5.jpg)

## License

YHWallet 使用 MIT 许可证，详情见 LICENSE 文件。
